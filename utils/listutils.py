#-*- coding: utf-8 -*-


def move_to_first(filelist, name):
    name = name.decode('utf8').encode('cp1250')
    filelist.remove(name)
    filelist.insert(0, name)

def move_to_last(filelist, name):
    name = name.decode('utf8').encode('cp1250')
    filelist.remove(name)
    filelist.append(name)


