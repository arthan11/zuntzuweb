#-*- coding: utf-8 -*-
import shutil
from os import listdir, mkdir, remove
from os.path import isfile, join, exists

from utils.listutils import move_to_last


def get_filelist(folder, ext=None):
    #print folder
    if ext != None:
        return [f for f in listdir(folder) if (isfile(join(folder, f)) and f.endswith('.'+ext)) ]
    else:
        return [f for f in listdir(folder) if isfile(join(folder, f))]

def get_count_from_item(item):
    return item['ilosc']

def get_filelist_with_dubles(folder, counts_filename):
    table = [f for f in listdir(folder) if isfile(join(folder, f))]

    with open(counts_filename) as f:
        duble = f.readlines()

    tab_ilosc = []
    for line in duble:
        tab = line.split()
        mnoznik = tab[-1]
        nazwa = (' ').join(tab[:-1])
        if mnoznik[0] == '+':
            ile = int(mnoznik[1:])
            #for i in range(ile):
            #    filelist.append(nazwa+'.jpg')
            tab_ilosc.append({'nazwa': nazwa+'.jpg', 'ilosc':ile+1})
    tab_ilosc = sorted(tab_ilosc, key=get_count_from_item)
    for ilosc in tab_ilosc:
        move_to_last(table, ilosc['nazwa'])
    return table, tab_ilosc

def change_ext(filename, new_ext):
    return filename[:filename.rfind('.')+1] + new_ext

def filename_without_ext(filename):
    return filename[:filename.rfind('.')]

def empty_folder(folder):
    if exists(folder):
        shutil.rmtree(folder)
    mkdir(folder)

def copy_file(src, dst):
    shutil.copyfile(src, dst)

def delete_file(filename):
    if exists(filename):
        remove(filename)