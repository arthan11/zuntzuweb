import os
import sys
import zipfile
import ntpath
import django
from os.path import join, dirname
import xml.etree.cElementTree as ET
from utils.fileutils import *

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
django.setup()
from zuntzu.models import *


class GameBoxImporter(object):

    def __init__(self):
        pass


    def attrib(self, attrib):
        return self.root.attrib[attrib]

    def unzip_file(self, filename, folder):
        with zipfile.ZipFile(filename, "r") as z:
            z.extractall(folder)

    def print_dodano(self, object):
        #print('Dodano [{}]: {}'.format(object.__class__.__name__, object))
        pass

    def get_resolution(self, xml, name, default=150):
        if name in xml.attrib:
            return xml.attrib[name][:-4]
        else:
            return 150

    def add_maps(self, game):
        for xml_map in self.root.findall('map'):
            db_map = Map()
            db_map.name = xml_map.attrib['name']
            db_map.game = game
            db_map.image_file = xml_map.attrib['image-file']
            db_map.resolution = self.get_resolution(xml_map, 'resolution')
            db_map.save()
            self.print_dodano(db_map)

    def add_dice_hands(self, game):
        for xml_dice_hand in self.root.findall('dice-hand'):
            db_dice_hand = DiceHand()
            db_dice_hand.game = game
            dice_type = xml_dice_hand.attrib['type']
            dice_type = int(dice_type[1:])
            db_dice_hand.dice_type = dice_type
            db_dice_hand.save()
            self.print_dodano(db_dice_hand)

            for xml_dice in xml_dice_hand.findall('dice'):
                db_dice = Dice()
                db_dice.hand = db_dice_hand
                if 'count' in xml_dice.attrib:
                    db_dice.count = xml_dice.attrib['count']
                if 'texture-file' in xml_dice.attrib:
                    db_dice.texture_file = xml_dice.attrib['texture-file']
                db_dice.save()
                self.print_dodano(db_dice)

    def do_import(self, ztb_path):
        ztb_filename = ntpath.basename(ztb_path)
        folder_name = filename_without_ext(ztb_filename)
        #folder = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        #folder = join(folder, 'mysite', 'static', 'modules', 'data', folder_name)
        folder = dirname(dirname(ztb_path))
        folder = join(folder, 'data', folder_name)
        print(folder)

        empty_folder(folder)
        self.unzip_file(ztb_path, folder)

        tree = ET.parse(join(folder, 'game-box.xml'))
        self.root = tree.getroot()

        game = Game.objects.filter(name=self.attrib('name'))
        if game:
            print('usunieto')
            game.delete()

        db_game = Game()
        db_game.name = self.attrib('name')
        db_game.copyright = self.attrib('copyright')
        db_game.description = self.attrib('description')
        db_game.folder = folder_name
        #game.startup_scenario = self.attrib('startup-scenario')
        db_game.save()
        self.print_dodano(db_game)

        self.add_maps(db_game)
        self.add_dice_hands(db_game)
        self.add_sheets(db_game)
        self.add_scenarios(folder, db_game)
        self.set_default_scenario(db_game)

    def set_default_scenario(self, db_game):
        scenario_file = self.attrib('startup-scenario')
        db_scenario = Scenario.objects.get(game=db_game, scenario_file=scenario_file)
        db_game.startup_scenario = db_scenario
        db_game.save()

    def add_scenarios(self, folder, game):
        zts_files = get_filelist(folder, 'zts')

        for zts_file in zts_files:
            tree = ET.parse(join(folder,zts_file))
            xml_scenario = tree.getroot()

            #game = Game.objects.filter(name=self.attrib('game-box'))
            db_scenario = Scenario()
            db_scenario.name = xml_scenario.attrib['scenario-name']
            db_scenario.game = game
            if 'scenario-copyright' in xml_scenario.attrib:
                db_scenario.copyright = xml_scenario.attrib['scenario-copyright']
            if 'scenario-description' in xml_scenario.attrib:
                db_scenario.description = xml_scenario.attrib['scenario-description']
            db_scenario.scenario_file = zts_file
            db_scenario.save()
            self.print_dodano(db_scenario)
            for xml_layout in xml_scenario.findall('layout'):
                db_layout = Layout()
                db_layout.scenario = db_scenario
                db_layout.board_name = xml_layout.attrib['board']
                if 'left' in xml_layout.attrib:
                    db_region = Region()
                    db_region.game = game
                    db_region.left = xml_layout.attrib['left']
                    db_region.top = xml_layout.attrib['top']
                    db_region.right = xml_layout.attrib['right']
                    db_region.bottom = xml_layout.attrib['bottom']
                    db_region.save()
                    db_layout.region = db_region

                map = Map.objects.filter(name=db_layout.board_name)
                if map:
                    db_layout.map = map[0]

                sheet = TerrainSheet.objects.filter(name=db_layout.board_name)
                if sheet:
                    db_layout.terrain_sheet = sheet[0]

                sheet = CounterSheet.objects.filter(name=db_layout.board_name)
                if sheet:
                    db_layout.counter_sheet = sheet[0]

                db_layout.save()
                self.print_dodano(db_layout)


    def add_sheets(self, game):
        for xml_sheet in self.root.findall('terrain-sheet') + self.root.findall('counter-sheet'):
            if xml_sheet.tag == 'terrain-sheet':
                db_sheet = TerrainSheet()
            elif xml_sheet.tag == 'counter-sheet':
                db_sheet = CounterSheet()
            db_sheet.name = xml_sheet.attrib['name']
            db_sheet.game = game

            db_front_image = SheetImage()
            db_front_image.game = game
            db_front_image.image_file = xml_sheet.attrib['front-image-file']
            db_front_image.resolution = self.get_resolution(xml_sheet, 'front-resolution')
            if 'front-mask-file' in xml_sheet.attrib:
                db_front_image.mask_file = xml_sheet.attrib['front-mask-file']
            db_front_image.save()
            self.print_dodano(db_front_image)
            db_sheet.front_image = db_front_image

            if 'back-image-file' in xml_sheet.attrib:
                if xml_sheet.attrib['back-image-file'] != xml_sheet.attrib['front-image-file']:
                    db_back_image = SheetImage()
                    db_back_image.game = game
                    db_back_image.image_file = xml_sheet.attrib['back-image-file']
                    db_back_image.resolution = self.get_resolution(xml_sheet, 'back-resolution')
                    if 'back-mask-file' in xml_sheet.attrib:
                        db_back_image.mask_file = xml_sheet.attrib['back-mask-file']
                    db_back_image.save()
                    self.print_dodano(db_back_image)
                    db_sheet.back_image = db_back_image
                else:
                    db_sheet.back_image = db_front_image

            db_sheet.save()
            self.print_dodano(db_sheet)

            sheets = xml_sheet.findall('terrain-section') + xml_sheet.findall('counter-section') + xml_sheet.findall('card-section')
            for xml_section in sheets:
                if xml_section.tag == 'terrain-section':
                    db_section = TerrainSection()
                elif xml_section.tag == 'counter-section':
                    db_section = CounterSection()
                elif xml_section.tag == 'card-section':
                    db_section = CardSection()
                else:
                    print('zly typ sekcji')
                    sys.exit()

                #print( db_section )
                db_section.sheet = db_sheet
                if 'columns' in xml_section.attrib:
                    db_section.columns = xml_section.attrib['columns']
                if 'rows' in xml_section.attrib:
                    db_section.rows = xml_section.attrib['rows']
                if 'supply' in xml_section.attrib:
                    db_section.supply = xml_section.attrib['supply']

                if 'front-left' in xml_section.attrib:
                    db_region = Region()
                    db_region.game = game
                    db_region.left = xml_section.attrib['front-left']
                    db_region.top = xml_section.attrib['front-top']
                    db_region.right = xml_section.attrib['front-right']
                    db_region.bottom = xml_section.attrib['front-bottom']
                    db_region.save()
                    self.print_dodano(db_region)
                    db_section.front = db_region

                if 'face-left' in xml_section.attrib:
                    db_region = Region()
                    db_region.game = game
                    db_region.left = xml_section.attrib['face-left']
                    db_region.top = xml_section.attrib['face-top']
                    db_region.right = xml_section.attrib['face-right']
                    db_region.bottom = xml_section.attrib['face-bottom']
                    db_region.save()
                    self.print_dodano(db_region)
                    db_section.face = db_region

                if 'back-left' in xml_section.attrib:
                    db_region = Region()
                    db_region.game = game
                    db_region.left = xml_section.attrib['back-left']
                    db_region.top = xml_section.attrib['back-top']
                    db_region.right = xml_section.attrib['back-right']
                    db_region.bottom = xml_section.attrib['back-bottom']
                    db_region.save()
                    self.print_dodano(db_region)
                    db_section.back = db_region

                db_section.save()
                self.print_dodano(db_section)


if __name__ == '__main__':
    importer = GameBoxImporter()
    #importer.do_import('GhostStories_v1.0.ztb')

    folder = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + r'/mysite/static/modules/ztb'
    ztb_files = get_filelist(folder, 'ztb')
    print(ztb_files)
    for ztb_file in [ztb_files[0]]:
        game_folder_name = filename_without_ext(ztb_file)
        #print( game_folder_name )
        game = Game.objects.filter(folder=game_folder_name)
        if False:#game:
            print( u'{} juz istnieje'.format(game[0].name) )
        else:
            print( 'dodawanie gry: {}'.format(game_folder_name) )
            importer.do_import(join(folder, ztb_file))