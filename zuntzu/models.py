from django.db import models
from os.path import exists, join, dirname, abspath
#from django.contrib.auth.models import User
from zuntzu.abs_models import *

class Game(Named, CopyDesc):
    folder = models.CharField(max_length=200)
    startup_scenario = models.ForeignKey('Scenario', blank=True, null=True, related_name='startup_game')
    def icon_exists(self):
        base_folder = dirname(dirname(abspath(__file__)))
        icon_file = join(base_folder, 'static', 'modules', 'data', self.folder, 'icon.bmp')
        #print(icon_file)
        return exists(icon_file)
        #'icon_default.bmp'
    class Meta:
        ordering = ['name']
class Scenario(Named, GameRelated, CopyDesc):
    scenario_file = models.CharField(max_length=200)

class Layout(models.Model):
    board_name = models.CharField(max_length=200)
    scenario = models.ForeignKey('Scenario')
    region = models.ForeignKey('Region', blank=True, null=True)
    map = models.ForeignKey('Map', blank=True, null=True)
    terrain_sheet = models.ForeignKey('TerrainSheet', blank=True, null=True)
    counter_sheet = models.ForeignKey('CounterSheet', blank=True, null=True)

class Stack(models.Model):
    layout = models.ForeignKey('Layout')
    x = models.FloatField()
    y = models.FloatField()

class Counter(models.Model):
    layout = models.ForeignKey('Layout')
    stack = models.ForeignKey('Stack', blank=True, null=True)
    counter_id = models.IntegerField()
    x = models.FloatField()
    y = models.FloatField()
    back = models.BooleanField(default=False)

class Map(Named, GameRelated):
    image_file = models.CharField(max_length=200)
    resolution = models.IntegerField(default=150)

class SheetImage(GameRelated):
    image_file = models.CharField(max_length=200)
    resolution = models.IntegerField(default=150)
    mask_file = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return u'{}: {}'.format(self.game.name, self.image_file)

class TerrainSheet(Sheet):
    pass

class CounterSheet(Sheet):
    pass

class Region(GameRelated):
    left = models.FloatField()
    top = models.FloatField()
    right = models.FloatField()
    bottom = models.FloatField()
    def __str__(self):
        return u'[({}, {}), ({}, {})]'.format(self.left, self.top, self.right, self.bottom)

class TerrainSection(ColsRows):
    sheet = models.ForeignKey(TerrainSheet)
    front = models.ForeignKey(Region, related_name='front_terrain_section')
    back = models.ForeignKey(Region, related_name='back_terrain_section', blank=True, null=True)
    supply = models.IntegerField(default=1)

class CounterSection(Section):
    front = models.ForeignKey(Region, related_name='%(app_label)s_%(class)s_front_ownership')

class CardSection(Section):
    face = models.ForeignKey(Region, related_name='%(app_label)s_%(class)s_face_ownership')

class DiceHand(GameRelated):
    dice_type = models.IntegerField()
    #dice_mod = models.IntegerField()
    def __str__(self):
        return u'{}: D{}'.format(self.game.name, self.dice_type)

class Dice(models.Model):
    hand = models.ForeignKey(DiceHand)
    count = models.IntegerField(default=1)
    texture_file = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        txt = u'{} [{}]'.format(self.hand, self.count)
        if self.texture_file:
            txt += u' {}'.format(self.texture_file)
        return txt