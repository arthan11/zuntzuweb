from django.contrib import admin
from zuntzu.models import *


admin.site.register(Game)
admin.site.register(Scenario)
admin.site.register(Layout)
admin.site.register(Stack)
admin.site.register(Counter)
admin.site.register(Map)
admin.site.register(SheetImage)
admin.site.register(TerrainSheet)
admin.site.register(CounterSheet)
admin.site.register(Region)
admin.site.register(TerrainSection)
admin.site.register(CounterSection)
admin.site.register(CardSection)
admin.site.register(DiceHand)
admin.site.register(Dice)



