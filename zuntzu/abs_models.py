from django.db import models


class Named(models.Model):
    name = models.CharField(max_length=200)
    class Meta:
        abstract = True
    def __str__(self):
        return self.name

class GameRelated(models.Model):
    game = models.ForeignKey('Game')
    class Meta:
        abstract = True

class ColsRows(models.Model):
    columns = models.IntegerField(default=1)
    rows = models.IntegerField(default=1)
    class Meta:
        abstract = True

class CopyDesc(models.Model):
    copyright = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    class Meta:
        abstract = True

class Sheet(Named, GameRelated):
    front_image = models.ForeignKey('SheetImage', related_name='%(app_label)s_%(class)s_front_ownership')
    back_image = models.ForeignKey('SheetImage', related_name='%(app_label)s_%(class)s_back_ownership', blank=True, null=True)
    class Meta:
        abstract = True

class Section(ColsRows):
    sheet = models.ForeignKey('CounterSheet')
    back = models.ForeignKey('Region', related_name='%(app_label)s_%(class)s_back_ownership', blank=True, null=True)
    supply = models.IntegerField(default=1)
    class Meta:
        abstract = True
