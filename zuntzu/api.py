from rest_framework import routers, serializers, viewsets
from zuntzu.models import *


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = '__all__'

class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    filter_fields = ('id', )

class ScenarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scenario
        fields = '__all__'

class ScenarioViewSet(viewsets.ModelViewSet):
    queryset = Scenario.objects.all()
    serializer_class = ScenarioSerializer
    filter_fields = ('id', 'game')

class LayoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = '__all__'

class LayoutViewSet(viewsets.ModelViewSet):
    queryset = Layout.objects.all()
    serializer_class = LayoutSerializer
    filter_fields = ('id', 'scenario')

class StackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stack
        fields = '__all__'

class StackViewSet(viewsets.ModelViewSet):
    queryset = Stack.objects.all()
    serializer_class = StackSerializer
    filter_fields = ('id', 'layout')

class CounterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Counter
        fields = '__all__'

class CounterViewSet(viewsets.ModelViewSet):
    queryset = Counter.objects.all()
    serializer_class = CounterSerializer
    filter_fields = ('id', 'layout', 'stack')

class MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Map
        fields = '__all__'

class MapViewSet(viewsets.ModelViewSet):
    queryset = Map.objects.all()
    serializer_class = MapSerializer
    filter_fields = ('id', 'game')

class SheetImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = SheetImage
        fields = '__all__'

class SheetImageViewSet(viewsets.ModelViewSet):
    queryset = SheetImage.objects.all()
    serializer_class = SheetImageSerializer
    filter_fields = ('id', 'game')

class TerrainSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = TerrainSheet
        fields = '__all__'

class TerrainSheetViewSet(viewsets.ModelViewSet):
    queryset = TerrainSheet.objects.all()
    serializer_class = TerrainSheetSerializer
    filter_fields = ('id', 'game')

class CounterSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = CounterSheet
        fields = '__all__'

class CounterSheetViewSet(viewsets.ModelViewSet):
    queryset = CounterSheet.objects.all()
    serializer_class = CounterSheetSerializer
    filter_fields = ('id', 'game')

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'

class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    filter_fields = ('id', )

class TerrainSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TerrainSection
        fields = '__all__'

class TerrainSectionViewSet(viewsets.ModelViewSet):
    queryset = TerrainSection.objects.all()
    serializer_class = TerrainSectionSerializer
    filter_fields = ('id', 'sheet')


class CounterSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CounterSection
        fields = '__all__'

class CounterSectionViewSet(viewsets.ModelViewSet):
    queryset = CounterSection.objects.all()
    serializer_class = CounterSectionSerializer
    filter_fields = ('id', 'sheet')

class CardSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardSection
        fields = '__all__'

class CardSectionViewSet(viewsets.ModelViewSet):
    queryset = CardSection.objects.all()
    serializer_class = CardSectionSerializer
    filter_fields = ('id', 'sheet')

class DiceHandSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiceHand
        fields = '__all__'

class DiceHandViewSet(viewsets.ModelViewSet):
    queryset = DiceHand.objects.all()
    serializer_class = DiceHandSerializer
    filter_fields = ('id', 'game')

class DiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dice
        fields = '__all__'

class DiceViewSet(viewsets.ModelViewSet):
    queryset = Dice.objects.all()
    serializer_class = DiceSerializer
    filter_fields = ('id', 'hand')



zuntzu_routers = [
    [r'games', GameViewSet],
    [r'scenarios', ScenarioViewSet],
    [r'layouts', LayoutViewSet],
    [r'stacks', StackViewSet],
    [r'counters', CounterViewSet],
    [r'maps', MapViewSet],
    [r'sheetimages', SheetImageViewSet],
    [r'terrainsheets', TerrainSheetViewSet],
    [r'countersheets', CounterSheetViewSet],
    [r'regions', RegionViewSet],
    [r'terrainsections', TerrainSectionViewSet],
    [r'countersections', CounterSectionViewSet],
    [r'countersections', CounterSectionViewSet],
    [r'cardsections', CardSectionViewSet],
    [r'dicehands', DiceHandViewSet],
    [r'dices', DiceViewSet],


]

