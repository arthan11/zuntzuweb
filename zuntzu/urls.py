"""zuntzuweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from zuntzu import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^list_games$', views.list_games),
    url(r'^upload_module$', views.upload_module),
    url(r'^game/(?P<game_id>\d+)$', views.game_by_id),
    url(r'^game/(?P<game_id>\d+)/gamebox$', views.gamebox_by_id),
    url(r'^game_delete/(?P<game_id>\d+)$', views.game_delete),
    url(r'^imgs_of_game/(?P<game_id>\d+)$', views.imgs_of_game),
    url(r'^pokemon_list$', views.pokemon_list),
]
