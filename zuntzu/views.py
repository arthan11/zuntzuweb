import json
from os.path import join, dirname, abspath
from django.shortcuts import render_to_response
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from zuntzu.models import *
from zuntzu.api import *
from utils.gamebox_import import GameBoxImporter
from PIL import Image

def pokemon_list(request):
    params = []
    return render_to_response('poke_list.html', params)

def index(request):
    params = []
    return render_to_response('index.html', params)

def list_games(request):
    params = {}
    games = []
    db_games = Game.objects.all()
    games = db_games
    #for db_game in db_games:
    #    print(db_game.name)
    #    print(db_game.icon_exists())
    #    games.append({'name': db_game)
    params['games'] = games
    return render_to_response('list_games.html', params)

def imgs_of_game(request, game_id):
    imgs = []
    response_data = {'imgs': imgs}
    db_game = Game.objects.filter(id=game_id)
    if not db_game:
        return JsonResponse(response_data)
    else:
        db_game = db_game[0]

    db_maps = db_game.map_set.all()
    for db_map in db_maps:
        img = db_game.folder + '/' + db_map.image_file
        if not img in imgs:
            imgs.append(img)

    db_sheet_images = db_game.sheetimage_set.all()
    for db_sheet_image in db_sheet_images:
        img = db_game.folder + '/' + db_sheet_image.image_file
        if not img in imgs:
            imgs.append(img)
        img = db_sheet_image.mask_file
        if img:
            img = db_game.folder + '/' + img
            if not img in imgs:
                imgs.append(img)

    return JsonResponse(response_data)

def game_by_id(request, game_id):
    response_data = {}
    db_game = Game.objects.filter(id=game_id)
    if not db_game:
        return JsonResponse(response_data)
    else:
        db_game = db_game[0]

    db_scenarios = db_game.scenario_set.all()
    scenarios = []
    for db_scenario in db_scenarios:
        scenario = {
            'id': db_scenario.id,
            'name': db_scenario.name,
            'description': db_scenario.description,
            'copyright': db_scenario.copyright
        }
        scenarios.append(scenario)
    response_data['scenarios'] = scenarios
    response_data['startup_scenario'] = db_game.startup_scenario.id

    return JsonResponse(response_data)

def handle_uploaded_file(f):
    base_folder = dirname(dirname(abspath(__file__)))
    folder = join(base_folder, 'static', 'modules', 'ztb')
    filename = join(folder, f.name)
    with open(filename, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    importer = GameBoxImporter()
    importer.do_import(filename)

@csrf_exempt
def upload_module(request):
    if request.method == 'POST':
        handle_uploaded_file(request.FILES['file'])
        return HttpResponse('ok')
    return HttpResponse('fail')

@login_required
def game_delete(request, game_id):
    db_game = Game.objects.filter(id=game_id)
    if db_game:
        db_game.delete()
    return HttpResponse('ok')

def get_img_size(folder, image_file):
    base_folder = dirname(dirname(abspath(__file__)))
    img_path = join(base_folder, 'static', 'modules', 'data', folder, image_file)
    img = Image.open(img_path)
    x, y = img.size
    img.close()
    return x, y

def gamebox_by_id(request, game_id):
    data = {}
    db_game = Game.objects.filter(id=game_id)
    if not db_game:
        return JsonResponse(data)
    else:
        db_game = db_game[0]

    dice_hands = []
    db_dice_hands = DiceHand.objects.filter(game=db_game)
    for db_dice_hand in db_dice_hands:
        dice_hand = DiceHandSerializer(db_dice_hand).data
        del dice_hand['game']

        dices = []
        db_dices = Dice.objects.filter(hand=db_dice_hand)
        for db_dice in db_dices:
            dice = DiceSerializer(db_dice).data
            del dice['hand']
            dices.append(dice)
        dice_hand['dices'] = dices

        dice_hands.append(dice_hand)

    maps = []
    db_maps = Map.objects.filter(game=db_game)
    for db_map in db_maps:
        map = MapSerializer(db_map).data
        del map['game']
        x, y = get_img_size(db_game.folder, map['image_file'])
        map['width'] = x
        map['height'] = y
        maps.append(map)

    countersheets = []
    db_countersheets = CounterSheet.objects.filter(game=db_game)
    for db_countersheet in db_countersheets:
        countersheet = CounterSheetSerializer(db_countersheet).data
        del countersheet['game']
        if countersheet['front_image']:
            db_front_image = SheetImage.objects.filter(id=countersheet['front_image'])[0]
            image = SheetImageSerializer(db_front_image).data
            countersheet['front_image'] = image
            del image['game']
            x, y = get_img_size(db_game.folder, image['image_file'])
            image['width'] = x
            image['height'] = y
        countersheets.append(countersheet)


    data['game'] = GameSerializer(db_game).data
    data['dice_hands'] = dice_hands
    data['maps'] = maps
    data['countersheets'] = countersheets

    return JsonResponse(data)

