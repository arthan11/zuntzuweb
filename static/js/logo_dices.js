var logo_dices = null;

LogoDices = function(parent) {
  logo_dices = this;
  this.parent = $(parent);
  this.object = null;

  this.create_logo_dices = function() {

    this.tittle_screen = $(`
      <div class="tittle_screen">
        <div class="app_title asia_font">ZUNTZU WEB</div>
        <div class="app_version">0.1 alpha</div>
        <div class="app_version">Copyright&copy; Arthan Software 2016</div>
      </div>
    `);

    this.object = $(`
      <div id="wrapper">
        <div id="platform">
          <div class="dice" id="dice1">
            <div class="side front">
              <div class="txt asia_font">Z</div>
            </div>
            <div class="side front inner"></div>
            <div class="side top"></div>
            <div class="side top inner"></div>
            <div class="side right">
              <div class="txt asia_font">U</div>
            </div>
            <div class="side right inner"></div>
            <div class="side left"></div>
            <div class="side left inner"></div>
            <div class="side bottom">
              <div class="txt asia_font">N</div>
            </div>
            <div class="side bottom inner"></div>
            <div class="side back"></div>
          </div>

          <div class="dice" id="dice2">
            <div class="side front">
              <div class="txt asia_font">T</div>
            </div>
            <div class="side front inner"></div>
            <div class="side top"></div>
            <div class="side top inner"></div>
            <div class="side right">
              <div class="txt asia_font">Z</div>
            </div>
            <div class="side right inner"></div>
            <div class="side left"></div>
            <div class="side left inner"></div>
            <div class="side bottom">
              <div class="txt asia_font">U</div>
            </div>
            <div class="side bottom inner"></div>
            <div class="side back"></div>
          </div>

          <div class="dice" id="dice3">
            <div class="side front">
              <div class="txt asia_font">W</div>
            </div>
            <div class="side right">
              <div class="txt asia_font">E</div>
            </div>
            <div class="side bottom">
              <div class="txt asia_font">B</div>
            </div>
          </div>

        </div>
      </div>
    `);
    this.object.appendTo(this.parent);
    this.tittle_screen.appendTo(this.parent);

  };

  this.create_logo_dices();
};
