pos1 = [0, 0];
  
  $(function() {
    /*$('#box1').click( function(){
        $('#box1').velocity({
            rotateZ: "+=45deg"
        });
      }
    );*/
    
	$( "#tabs" ).tabs();//{heightStyle: "fill"});
	
    $('.box').mousewheel(function(event) {
      var deg = parseInt($(this).attr('deg'));
      var scale = parseFloat($(this).attr('scale'));
      if (isNaN(scale)) {
        scale = 1.0;
      };
      if (isNaN(deg)) {
        deg = 0;
      };
      deg = deg + event.deltaY*15*-1;
      $(this).attr('deg', deg);
      $(this).css("transform", "scale("+scale+") rotateZ("+deg+"deg)");
	  //$('#board').css("transform", "perspective(0px) translateZ("+deg+"px)");
    });


    element_onmove = function(event) {
      var pos2 = [event.pageX, event.pageY];
      var left = parseInt($('#box1').css("left")) + (pos2[0] - pos1[0]);
      var top = parseInt($('#box1').css("top")) + (pos2[1] - pos1[1]);
      pos1 = [event.pageX, event.pageY];
      $('#box1').css("left", left+"px");
      $('#box1').css("top", top+"px");
    };    
    
    element_onmouseup = function(event) {
      element_onmove(event);
      $("#board").unbind();
    };
    
    $('#box1').mousedown(function(event) {
      if (event.which == 1) {
        pos1 = [event.pageX, event.pageY];
        $('#board').mousemove(element_onmove);
        $('#board').mouseup(element_onmouseup);
      };
    });
    
   
  });
