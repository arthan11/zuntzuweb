var loading = null;

Loading = function(parent, txt) {
  loading = this;
  this.parent = $(parent);
  this.object = null;
  this.value = 0;
  this.max = 100;

  this.create_loading = function(txt) {
    this.object = $('<div class="loading"><div class="txt">'+txt+'</div><div class="progressbar"><div class="status"></div></div></div>');
    this.object.appendTo(this.parent);
  };

  this.set_progress = function(value) {
    this.value = value;
    value = value / this.max * 294;
    $('.loading .progressbar .status').css('width', value);
  };

  this.destroy = function() {
    //alert('done');
    //alert(this.max);
    //alert(this.value);
    this.object.remove();
    loading = null;
  };

  this.create_loading(txt)
  this.set_progress(0);
};
