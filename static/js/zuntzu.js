load_game = function(game_id, game_name){
  //alert( game_id );
  logo_dices.object.hide();
  logo_dices.tittle_screen.hide();
  new Table('#table', game_id);

  $('#imgs_container').empty();

  $.get( "/imgs_of_game/"+game_id, function( data ) {
    var img_list = data['imgs'];
    new Loading('body', 'Loading graphics...');
    loading.value = 0;
    loading.max = img_list.length;
    for (i in img_list) {
      var img_path = "/static/modules/data/"+img_list[i];
      var img = $('<img src="'+ img_path +'">');
      img.appendTo('#imgs_container');
    };
    $('#imgs_container').imagesLoaded( function() {
      //alert('done');
      loading.destroy();
    }).progress( function( instance, image ) {
      var result = image.isLoaded ? 'loaded' : 'broken';
      loading.set_progress(loading.value + 1);
    });


  });

};

$(function() {

  Dropzone.autoDiscover = false;
  var myDropzone = new Dropzone("#ztb_uploader", {
    maxFiles: 1,
    acceptedFiles: ".ztb"
  });
  myDropzone.on("complete", function(file) {
    $("#dialog_game_add").dialog( "close" );
    $( "#dialog_game_list" ).dialog( "close" );
    $( "#dialog_game_list" ).dialog( "open" );
  });

  $("html").on("contextmenu",function(){
     return false;
  });

  $('html').mousedown(function(event) {
    if (event.which == 1) {
    }
    else if (event.which == 3) {
      $('body').css('cursor', 'url(\'/static/img/magnifying glass.cur\'), default');
    };
  });

  $('html').mouseup(function(event) {
    if (event.which == 3) {
      $('body').css('cursor', 'grab')
    };
  });

  $( "#dialog_about" ).dialog({
    dialogClass: 'noTitle',
    autoOpen: false,
    resizable: false,
    height: 200,
    width: 340,
    modal: true,
    buttons: {
      Close: function() {
        $( this ).dialog( "close" );
      }
    }
  });

  $( "#dialog_game_add" ).dialog({
    dialogClass: 'noTitle',
    autoOpen: false,
    resizable: false,
    height: 340,
    width: 340,
    modal: true,
    close: function( event, ui ) {
        Dropzone.forElement("#ztb_uploader").removeAllFiles(true);
    },
    buttons: {
      Close: function() {
        $( this ).dialog( "close" );
      }
    }
  });


  $( "#dialog_game_list" ).dialog({
    dialogClass: 'noTitle',
    autoOpen: true,
    resizable: false,
    width: 800,
    height: 550,
    modal: true,
    open: function( event, ui ) {
      $.get( "/list_games", function( data ) {
        $( "#game_list" ).html( data );
        $( "#games" ).selectable();

        $( "#games li" ).dblclick(function() {
          $( "#dialog_game_list" ).dialog( "close" );
          load_game(this.id);
        });

      });
    },
    close: function( event, ui ) {
      $( "#game_list" ).html( '' );
    }
  });

  $( "#dialog_game_list button" ).button();

  $( "#game_list_add" )
    .click(function( event ) {
      $("#dialog_game_add").dialog( "open" );
    });

  $( "#game_list_remove" )
    .click(function( event ) {
      var selected = $("li.ui-selected");
      if (selected.length > 0) {
        $.get( "/game_delete/"+selected[0].id, function( data ) {
          $( "#dialog_game_list" ).dialog( "close" );
          $( "#dialog_game_list" ).dialog( "open" );
        });
      };

    });

  $( "#game_list_close" )
    .click(function( event ) {
      $( "#dialog_game_list" ).dialog( "close" );
    });

  $( "#game_list_open" )
    .click(function( event ) {
      var selected = $("li.ui-selected");
      if (selected.length > 0) {
        $( "#dialog_game_list" ).dialog( "close" );
        load_game(selected[0].id);
      };
    });

  $( "#dialog_controls" ).dialog({
    dialogClass: 'noTitle',
    autoOpen: false,
    resizable: false,
    height: 575,
    width: 550,
    modal: true,
    buttons: {
      Close: function() {
        $( this ).dialog( "close" );
      }
    }
  });


  $( "#dialog_player" ).dialog({
    dialogClass: 'noTitle',
    autoOpen: false,
    resizable: false,
    height: 250,
    width: 300,
    modal: true,
    buttons: {
      Close: function() {
        $( this ).dialog( "close" );
      }
    }
  });
  $( "#dialog_player_combobox" ).selectmenu();

  new Menu('body');
  //new Loading('body', 'Loading graphics...');
  new LogoDices('body');

  //logo_dices.object.hide();
  //logo_dices.tittle_screen.hide();

  //new Table('body');

  function close_window(url){
    var newWindow = window.open('', '_self', ''); //open the current window
    window.close(url);
  }

});
