var table = null;

Dice = function() {
  this.id = null;
  this.count = null;
  this.texture_file = null;
};

DiceHand = function() {
  this.id = null;
  this.dice_type = null;
  this.dices = [];
};

SheetImage = function() {
  this.id = null;
  this.image_file = null;
  this.resolution = null;
  this.mask_file = null;
  this.width = null;
  this.height = null;
};

CounterSheet = function() {
  this.id = null;
  this.name = null;
  this.front_image = null;
  this.back_image = null;

  this.init = function() {
    var li = $('<li><a href="#tabs-counters-'+this.id+'">'+this.name+'</a></li>');
    li.appendTo(table.ul);
    var div = $('<div id="tabs-counters-'+this.id+'"></div>');
    div.appendTo(table.object);

    var div_img = $('<div></div>');
    var img_path = '/static/modules/data/'+table.gamebox.folder+'/'+this.front_image.image_file;
    div_img.css({
      'width': this.front_image.width,
      'height': this.front_image.height,
      'background-image': 'url("' + img_path + '")'
    })
    div_img.appendTo(div);
  };
};

Map = function() {
  this.id = null;
  this.name = null;
  this.image_file = null;
  this.resolution = null;
  this.width = null;
  this.height = null;

  this.init = function() {
    var li = $('<li><a href="#tabs-maps-'+this.id+'">'+this.name+'</a></li>');
    li.appendTo(table.ul);
    var div = $('<div id="tabs-maps-'+this.id+'"></div>');
    div.appendTo(table.object);

    var div_img = $('<div></div>');
    var img_path = '/static/modules/data/'+table.gamebox.folder+'/'+this.image_file;
    div_img.css({
      'width': this.width,
      'height': this.height,
      'background-image': 'url("' + img_path + '")'
    })
    div_img.appendTo(div);
  };
};

Gamebox = function() {
  this.id = null;
  this.name = null;
  this.copyright = null;
  this.description = null;
  this.folder = null;
  this.startup_scenario = null;
  this.dice_hands = [];
  this.layouts = [];
  this.counters = [];

  this.load_data = function(id) {
    $.getJSON( "/game/" + id + "/gamebox", function( data ) {
        // gamebox
        var gamebox = table.gamebox;
        gamebox.id = data['game']['id'];
        gamebox.name = data['game']['name'];
        gamebox.copyright = data['game']['copyright'];
        gamebox.description = data['game']['description'];
        gamebox.folder = data['game']['folder'];
        gamebox.startup_scenario = data['game']['startup_scenario'];

        // dice hands and dices
        for (var i in data['dice_hands']) {
          var jsn_dice_hand = data['dice_hands'][i];
          var dice_hand = new DiceHand();

          dice_hand.id = jsn_dice_hand['id'];
          dice_hand.dice_type = jsn_dice_hand['dice_type'];

          table.gamebox.dice_hands.push(dice_hand);

          for (var j in jsn_dice_hand['dices']) {
            var jsn_dice = jsn_dice_hand['dices'][j];
            var dice = new Dice();

            dice.id = jsn_dice['id'];
            dice.count = jsn_dice['count'];
            dice.texture_file = jsn_dice['texture_file'];

            dice_hand.dices.push(dice);
          };
        };

        // maps
        for (var i in data['maps']) {
          var jsn_map = data['maps'][i];
          var map = new Map();

          map.id = jsn_map['id'];
          map.name = jsn_map['name'];
          map.image_file = jsn_map['image_file'];
          map.resolution = jsn_map['resolution'];
          map.width = jsn_map['width'];
          map.height = jsn_map['height'];
          map.init();

          table.gamebox.layouts.push(map);
        };

        // counter sheets
        for (var i in data['countersheets']) {
          var jsn_countersheet = data['countersheets'][i];
          var counter_sheet = new CounterSheet();

          counter_sheet.id = jsn_countersheet['id'];
          counter_sheet.name = jsn_countersheet['name'];
          if (!!jsn_countersheet['front_image']) {
            var jsn_image = jsn_countersheet['front_image'];
            var image = new SheetImage();
            image.id = jsn_image['id'];
            image.image_file = jsn_image['image_file'];
            image.resolution = jsn_image['resolution'];
            image.mask_file = jsn_image['mask_file'];
            image.width = jsn_image['width'];
            image.height = jsn_image['height'];
            counter_sheet.front_image = image;
          };
          if (!!jsn_countersheet['back_image']) {
            var jsn_image = jsn_countersheet['back_image'];
            var image = new SheetImage();
            image.id = jsn_image['id'];
            image.image_file = jsn_image['image_file'];
            image.resolution = jsn_image['resolution'];
            image.mask_file = jsn_image['mask_file'];
            image.width = jsn_image['width'];
            image.height = jsn_image['height'];
            counter_sheet.back_image = image;
          };

          counter_sheet.init();

          table.gamebox.layouts.push(counter_sheet);
        };

        table.object.tabs("refresh");
      });
  };
};

/*
Map = function(parent, image_file, name, resolution=150) {
  this.parent = $(parent);
  this.object = null;
  this.image_file = image_file;
  this.name = name;
  this.resolution = resolution;

  this.create_map = function() {
    this.object = $('<div class="map"></div>');
    this.object.appendTo(this.parent);
  };
};

Layout = function(parent, name) {
  this.parent = parent;
  this.object = null;
  this.name = name;

  this.create = function() {
    var layout_nr = this.parent.layouts.length + 1;
    alert(layout_nr);
    var li = $('<li><a href="#tabs-'+layout_nr+'">'+name+'</a></li>');
    li.appendTo(this.parent.ul);
    var div = $('<div id="tabs-'+layout_nr+'"></div>');
    div.appendTo(this.parent.object);
    this.parent.object.tabs("refresh");

    //this.object = $('<div class="layout"></div>');
    //this.object.appendTo(this.parent);

    this.parent.layouts.push(this);
  };

  this.create();
};
*/

Table = function(parent, game_id) {
  if (!!table) {
    table.destroy();
  };
  table = this;
  this.parent = $(parent);
  this.game_id = game_id;
  this.scenario_id = null;
  this.object = null;
  this.ul = null;
  this.layouts = [];
  this.gamebox = null;

  this.add_layout = function(layout_data) {
    var layout = new Layout(this, layout_data['board_name']);

    if (!!layout_data['map']) {
      $.getJSON( "/api/maps/?format=json&id=" + layout_data['map'] , function( data ) {
        //table.layout_by_id(XX);
        alert(data[0]['image_file']);
      });
    };
  };

  this.create_table = function() {
    this.object = $('<div id="tabs"></div>');
    this.object.appendTo(this.parent);
    this.ul = $('<ul></ul>');
    this.ul.appendTo(this.object);
    this.object.tabs();
  };

  this.destroy = function() {
    this.object.remove();
    delete(table);
    table = null;
  };

  this.open_scenario = function(scenario_id) {
    this.scenario_id = scenario_id;
    $.getJSON( "/api/layouts/?format=json&scenario=" + scenario_id, function( data ) {
      $.each( data, function( key, val ) {
        table.add_layout(val);
      });
    });
  };

  this.create_table();

  this.gamebox = new Gamebox();
  this.gamebox.load_data(table.game_id);

  /*
  $.getJSON( "/game/" + table.game_id + "/gamebox", function( data ) {
    table.scenario_id = data[0]['startup_scenario'];
    if (!table.scenario_id) {
      $.getJSON( "/api/scenarios/?format=json&game=" + table.game_id, function( data ) {
        table.open_scenario( data[0]['id'] );
      });
    } else {
     table.open_scenario( table.scenario_id );
    };
  });*/

};
