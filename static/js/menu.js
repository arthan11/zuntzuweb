var menu = null;

Menu = function(parent) {
  menu = this;
  this.parent = $(parent);
  this.object = null;

  this.create_menu = function() {
    this.menu_icon = $('<div class="menu_icon"></div>');
    this.menu_icon.appendTo(this.parent);
    this.menu_icon.click(this.menu_icon_click);

    this.object = $('<div class="menu"></div>');
    this.object.appendTo(this.parent);
  };

  this.menu_icon_click = function(event) {
    menu.show_main_menu();
    menu.menu_icon.hide();
    event.stopPropagation();
  };

  this.create_element = function(nazwa, klasa, funkcja) {
    var element = $('<div class="item">'+nazwa+'</div>');
    if (klasa !== '') {
      $('<div class="'+klasa+'"></div>').appendTo(element);
    };
    element.click(funkcja);
    element.appendTo(this.object);
  };

  this.show_menu = function (names){
    elements = $('.menu .item');
    for (var i = 0; i < elements.length; i++) {
      var el = $(elements[i]);
      var el_name = el.text();
      if (names.indexOf(el_name) > -1) {
        el.show();
      } else {
        el.hide();
      };
    };
  };

  this.disable_element = function (names){
    elements = $('.menu .item');
    for (var i = 0; i < elements.length; i++) {
      var el = $(elements[i]);
      var el_name = el.text();
      if (names.indexOf(el_name) > -1) {
        el.addClass('disabled');
      } else {
        el.removeClass('disabled');
      };
    };
  };

  this.menu_plik = function (){
    menu.show_menu(['', 'Open File...', 'Browse Library...', 'Open Scenario...', 'Save As...', 'Save (Ctrl+S)', 'Quit'])
  };

  this.menu_multiplayer = function (){
    menu.show_menu(['', 'Host...', 'Connect...', 'Disconnect'])
  };

  this.menu_ustawienia = function (){
    menu.show_menu(['', 'Player...', 'Display...', 'Audio...', 'Video...'])
  };

  this.menu_pomoc = function (){
    menu.show_menu(['', 'Help...', 'About ZunTzu...'])
  };

  this.show_main_menu = function() {
    menu.show_menu(['File', 'Multiplayer', 'Settings', '?'])
  };

  this.menu_open = function() {
    //alert
  };

  this.menu_about = function() {
    $( "#dialog_about" ).dialog( "open" );
    menu.hide_all();
  };

  this.menu_help = function() {
    $( "#dialog_controls" ).dialog( "open" );
    menu.hide_all();
  };

  this.menu_player = function() {
    $( "#dialog_player" ).dialog( "open" );
    menu.hide_all();
  };

  this.menu_browse_library = function() {
    $( "#dialog_game_list" ).dialog( "open" );
    menu.hide_all();
  };

  this.hide_all = function() {
    menu.show_menu([]);
    menu.menu_icon.show();
  };

  this.create_menu();
  this.create_element('File', 'triangle', this.menu_plik);
  this.create_element('Multiplayer', 'triangle', this.menu_multiplayer);
  this.create_element('Settings', 'triangle', this.menu_ustawienia);
  this.create_element('?', 'triangle', this.menu_pomoc);
  this.create_element('', 'triangle_back', this.show_main_menu);
  this.create_element('Open File...', '', this.menu_open);
  this.create_element('Browse Library...', '', this.menu_browse_library);
  this.create_element('Open Scenario...', '', this.menu_open);
  this.create_element('Save As...', '', this.menu_open);
  this.create_element('Save (Ctrl+S)', '', this.menu_open);
  this.create_element('Quit', '', this.menu_open);
  this.create_element('Host...', '', this.menu_open);
  this.create_element('Connect...', '', this.menu_open);
  this.create_element('Disconnect', '', this.menu_open);
  this.create_element('Player...', '', this.menu_player);
  this.create_element('Display...', '', this.menu_open);
  this.create_element('Audio...', '', this.menu_open);
  this.create_element('Video...', '', this.menu_open);
  this.create_element('Help...', '', this.menu_help);
  this.create_element('About ZunTzu...', '', this.menu_about);
  this.disable_element(['Open Scenario...', 'Save As...', 'Save (Ctrl+S)', 'Disconnect'])
  this.hide_all();

  $('html').click(function() {
    menu.hide_all();
    //menu.menu_icon.show();
  });
  $('.menu .item').click(function(event){
      event.stopPropagation();
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) { // Escape
      if ($(menu.menu_icon).is(":visible")) {
        menu.menu_icon.toggle();
        menu.show_main_menu();
      } else {
        //menu.menu_icon.toggle();
        menu.hide_all();
      };
    }
  });

};
