import os
import django
from os.path import join
from xml.dom import minidom
import xml.etree.cElementTree as ET
from utils.fileutils import *

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "zuntzuweb.settings")
django.setup()
from zuntzu.models import *


class GameBoxMaker(object):

    def __init__(self):
        pass

    def attrib(self, attrib, value):
        self.root.attrib[attrib] = value

    def prettify(self, elem):
        rough_string = ET.tostring(elem, 'utf8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="\t")

    def add_maps(self):
        for map in self.game.map_set.all():
            el = ET.SubElement(self.root, "map", name=map.name)
            el.attrib['image-file'] = map.image_file
            el.attrib['resolution'] = '{} dpi'.format(map.resolution)

    def add_dice_hands(self):
        for db_dicehand in self.game.dicehand_set.all():
            xml_dicehand = ET.SubElement(self.root, "dice-hand")
            xml_dicehand.attrib['type'] = 'D{}'.format(db_dicehand.dice_type)
            for db_dice in db_dicehand.dice_set.all():
                xml_dice = ET.SubElement(xml_dicehand, "dice")
                xml_dice.attrib['count'] = str(db_dice.count)
                xml_dice.attrib['texture-file'] = str(db_dice.texture_file)




    def add_sheets(self):
        sheets = list(self.game.countersheet_set.all()) + list(self.game.terrainsheet_set.all())
        for sheet in sheets:
            if sheet.__class__ == CounterSheet:
                el = ET.SubElement(self.root, "counter-sheet", name=sheet.name)
                sections = list(sheet.countersection_set.all()) + list(sheet.cardsection_set.all())
            elif sheet.__class__ == TerrainSheet:
                el = ET.SubElement(self.root, "terrain-sheet", name=sheet.name)
                sections = sheet.terrainsection_set.all()
            el.attrib['front-image-file'] = sheet.front_image.image_file
            el.attrib['front-resolution'] = '{} dpi'.format(sheet.front_image.resolution)
            if sheet.front_image.mask_file:
                el.attrib['front-mask-file'] = sheet.front_image.mask_file
            if sheet.back_image:
                el.attrib['back-image-file'] = sheet.back_image.image_file
                el.attrib['back-resolution'] = '{} dpi'.format(sheet.back_image.resolution)
                if sheet.back_image.mask_file:
                    el.attrib['back-mask-file'] = sheet.back_image.mask_file

            for section in sections:
                if section.__class__ in [TerrainSection, CounterSection]:
                    if section.__class__ == CounterSection:
                        sec = ET.SubElement(el, "counter-section", rows=str(section.rows), columns=str(section.columns))
                    elif section.__class__ == TerrainSection:
                        sec = ET.SubElement(el, "terrain-section", rows=str(section.rows), columns=str(section.columns))
                    if section.front:
                        sec.attrib['front-left'] = str(section.front.left)
                        sec.attrib['front-top'] = str(section.front.top)
                        sec.attrib['front-right'] = str(section.front.right)
                        sec.attrib['front-bottom'] = str(section.front.bottom)
                    #if section.shadow != None:
                    #    sec.attrib['shadow'] = str(section.shadow)
                elif section.__class__ == CardSection:
                    sec = ET.SubElement(el, "card-section", rows=str(section.rows), columns=str(section.columns))
                    if section.face:
                        sec.attrib['face-left'] = str(section.face.left)
                        sec.attrib['face-top'] = str(section.face.top)
                        sec.attrib['face-right'] = str(section.face.right)
                        sec.attrib['face-bottom'] = str(section.face.bottom)
                if section.back:
                    sec.attrib['back-left'] = str(section.back.left)
                    sec.attrib['back-top'] = str(section.back.top)
                    sec.attrib['back-right'] = str(section.back.right)
                    sec.attrib['back-bottom'] = str(section.back.bottom)
                if section.supply != None:
                    sec.attrib['supply'] = str(section.supply)

    def make(self, game):
        folder = join(r'zuntzu\static\modules\data', game.folder)
        filename = 'game-box_new.xml'
        #empty_folder(folder)

        self.game = game
        self.root = ET.Element("game-box")
        self.attrib('name', game.name)
        self.attrib('description', game.description)
        self.attrib('copyright', game.copyright)
        self.attrib('startup-scenario', game.startup_scenario.scenario_file)

        self.add_maps()
        self.add_dice_hands()
        self.add_sheets()


        self.root = self.prettify(self.root)
        with open(join(folder, filename), 'w') as f:
            f.write(self.root)
        tree = ET.ElementTree(self.root)
        #tree.write(filename, encoding="utf-8", xml_declaration=True)


if __name__ == '__main__':
    gb_maker = GameBoxMaker()
    gb_maker.make(Game.objects.get(name='Ghost Stories'))